package stepDefinitions;

import org.openqa.selenium.WebDriver;

import commons.AbstractTest;
import cucumber.api.java.en.When;
import cucumberOptions.Hooks;
import pages.LoginPageObject;
import pages.PageFactoryManager;

public class LoginStepDefinitions extends AbstractTest {

	WebDriver driver;
	private LoginPageObject loginPage;
	public static String userName, password, loginPageUrl;

	public LoginStepDefinitions() {
		driver = Hooks.openBrowser();
		loginPage = PageFactoryManager.openLoginPage(driver);
	}

	@When("^I get login page URL$")
	public void iGetLoginPageUrl() {
		loginPageUrl = loginPage.getLoginPageURL();
	}

	@When("^I input to Username \"([^\"]*)\"$")
	public void iInputToUsername(String userName) {
		loginPage.inputEmailAddressTextbox(userName);
	}

	@When("^I input to Password \"([^\"]*)\"$")
	public void iInputToPassword(String password) {
		loginPage.inputPasswordTextbox(password);
	}

	@When("^I click to Login button$")
	public void iClickToLoginButton() {
		loginPage.clickLoginButton();
	}
	
	@When("^I click to Here link$")
	public void iClickToHereLink() {
		loginPage.clickHereLinkToOpenRegisterPage();
	}
}
