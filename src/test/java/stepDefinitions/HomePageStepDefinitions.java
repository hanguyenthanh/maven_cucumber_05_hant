package stepDefinitions;

import org.openqa.selenium.WebDriver;

import commons.AbstractTest;
import cucumber.api.java.en.Then;
import cucumberOptions.Hooks;
import pages.HomePageObject;
import pages.PageFactoryManager;

public class HomePageStepDefinitions extends AbstractTest {

	WebDriver driver;
	private HomePageObject homePage;
	public static String userName, password, loginPageUrl;

	public HomePageStepDefinitions() {
		driver = Hooks.openBrowser();
		homePage = PageFactoryManager.openHomePage(driver);
	}

	@Then("^I Verify Home page is displayed with message \"([^\"]*)\"$")
	public void iVerifyHomePageIsDisplayed(String message) {
		verifyTrue(homePage.isHomePageDisplayed(message));
	}
}
