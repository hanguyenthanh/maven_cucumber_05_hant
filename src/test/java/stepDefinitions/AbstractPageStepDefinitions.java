package stepDefinitions;

import org.openqa.selenium.WebDriver;

import commons.AbstractTest;
import cucumber.api.java.en.When;
import cucumberOptions.Hooks;
import pages.AbstractPageObject;
import pages.PageFactoryManager;

public class AbstractPageStepDefinitions extends AbstractTest {

	WebDriver driver;
	private AbstractPageObject abstractPage;

	public AbstractPageStepDefinitions() {
		driver = Hooks.openBrowser();
		abstractPage = PageFactoryManager.openAbstractPage(driver);
	}

	@When("^I input to \"([^\"]*)\" textbox with data \"([^\"]*)\"$")
	public void iInputToDynamicTexboxWithDynamicData(String textboxName, String inputValue) {
		abstractPage.inputToDynamicTextbox(textboxName, inputValue);
	}

	@When("^I input to \"([^\"]*)\" textbox with random data \"([^\"]*)\"$")
	public void iInputToDynamicTexboxWithRandomData(String textboxName, String inputValue) {
		inputValue = inputValue + " " + randomNumber();
		if (textboxName.contains("emailid"))
			inputValue = inputValue + "@gmail.com";
		abstractPage.inputToDynamicTextbox(textboxName, inputValue);
	}

	@When("^I input to \"([^\"]*)\" textarea with data \"([^\"]*)\"$")
	public void iInputToDynamicTexareaWithDynamicData(String textareaName, String inputValue) {
		abstractPage.inputToDynamicTextarea(textareaName, inputValue);
	}

	@When("^I input to \"([^\"]*)\" textarea with random data \"([^\"]*)\"$")
	public void iInputToDynamicTexareaWithRandomData(String textareaName, String inputValue) {
		inputValue = inputValue + " " + randomNumber();
		abstractPage.inputToDynamicTextarea(textareaName, inputValue);
	}

	@When("^I select item in \"([^\"]*)\" dropdown list with data \"([^\"]*)\"$")
	public void iSelectToDynamicDropdownListWithDynamicItem(String dropdownName, String item) {
		abstractPage.selectDynamicDropdownList(dropdownName, item);
	}

	@When("^I select \"([^\"]*)\" radio button$")
	public void iSelectRadioButtonWithDynamicData(String radioButtonID) {
		abstractPage.selectDynamicRadioButton(radioButtonID);
	}

	@When("^I verify message \"([^\"]*)\" is displayed$")
	public void iVerifyDynamicMessageIsDisplayed(String message) {
		abstractPage.isDynamicMessageDisplay(message);
	}

	@When("^I open \"([^\"]*)\" page$")
	public void iOpenDynamicPage(String pageName) {
		abstractPage.openDynamicPage(pageName);
	}

	@When("^I click to \"([^\"]*)\" button$")
	public void iClickToDynamicButton(String buttonName) {
		abstractPage.clickToDynamicButton(buttonName);
	}

	@When("^I wait for \"([^\"]*)\" seconds$")
	public void iWaitForSomeSeconds(String timeInSeconds) {
		abstractPage.sleep(timeInSeconds);

	}

	@When("^I make Date of birth textbox editable$")
	public void iMakeDateOfBirthTextboxEditable() {
		abstractPage.removeDateOfBirthBirthReadOnlyAttribute();
	}

	@When("^I input (first|second) Account ID$")
	public void iInputToAccountID(String account) {

		if (account.equalsIgnoreCase("first")) {

		}

		if (account.equalsIgnoreCase("second")) {

		}
	}
}