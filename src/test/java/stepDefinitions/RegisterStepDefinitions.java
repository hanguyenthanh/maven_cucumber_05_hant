package stepDefinitions;

import org.openqa.selenium.WebDriver;

import commons.AbstractTest;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumberOptions.Hooks;
import pages.PageFactoryManager;
import pages.RegisterPageObject;

public class RegisterStepDefinitions extends AbstractTest {

	WebDriver driver;
	private RegisterPageObject registerPage;
	public static String userName, password;

	public RegisterStepDefinitions() {
		driver = Hooks.openBrowser();
		registerPage = PageFactoryManager.openRegisterPage(driver);
	}

	@Given("^I open login page again$")
	public void iOpenLoginPageAgain() {
		registerPage.openLoginPageURL(LoginStepDefinitions.loginPageUrl);
	}

	@When("^I input to Email textbox with email as \"([^\"]*)\"$")
	public void iInputToEmailTextboxWithEmailAsSomething(String email) {
		registerPage.inputEmailTextbox(email + randomNumber() + "@gmail.com");
	}

	@Then("^I click to Submit button at Regiter page$")
	public void iClickToSubmitButtonAtRegisterPage() {
		registerPage.clickToSubmitButton();
	}

	@Then("^I get UserID info$")
	public void iGetUserIDInfo() {
		userName = registerPage.getUserIDText();
	}

	@Then("^I get Password info$")
	public void iGetPasswordInfo() {
		password = registerPage.getPasswordText();
	}
}
