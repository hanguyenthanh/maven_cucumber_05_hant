@demo
Feature: Login
  As a user
  I want to login to the system
  So that I can verify login function work well

  @register
  Scenario: Register to the application
    Given I get login page URL
    When I click to Here link
    And I input to Email textbox with email as "autotesting"
    And I click to Submit button at Regiter page
    Then I get UserID info
    And I get Password info

  @login
  Scenario: Login to system
    Given I open login page again 
    When I input to Username "mngr166551"
    And I input to Password "gumedev"
    And I click to Login button
    Then I Verify Home page is displayed with message "Welcome To Manager's Page of Guru99 Bank"

  @new_customer
  Scenario Outline: Create New Customer
    Given I open "New Customer" page
    When I input to "name" textbox with data "<Name>"
    And I select "<Gender>" radio button
    And I make Date of birth textbox editable
    And I input to "dob" textbox with data "<DateOfBirth>"
    And I input to "addr" textarea with data "<Address>"
    And I input to "city" textbox with data "<City>"
    And I input to "state" textbox with data "<State>"
    And I input to "pinno" textbox with data "<Pin>"
    And I input to "telephoneno" textbox with data "<Phone>"
    And I input to "emailid" textbox with random data "<Email>"
    And I input to "password" textbox with data "<Password>"
    And I click to "Submit" button
    Then I verify message "Customer Registered Successfully1!!!" is displayed

    Examples: New Customer Info
      | Name      | Gender | DateOfBirth | Address       | City | State   | Pin    | Phone      | Email    | Password    |
      | Ha Nguyen | f      | 1991-01-01  | 123 Somewhere | HCM  | Thu Duc | 123456 | 0123456789 | autotest | password123 |
