package pages;

import org.openqa.selenium.WebDriver;

import commons.AbstractPage;
import interfaces.AbstractPageUI;

public class AbstractPageObject extends AbstractPage {

	WebDriver driver;

	public AbstractPageObject(WebDriver mappingDriver) {

		driver = mappingDriver;
	}

	public void inputToDynamicTextbox(String textboxName, String inputValue) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_TEXTBOX, textboxName);
		sendkeyToElement(driver, AbstractPageUI.DYNAMIC_TEXTBOX, inputValue, textboxName);
	}

	public void inputToDynamicTextarea(String textareaName, String inputValue) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_TEXTAREA, textareaName);
		sendkeyToElement(driver, AbstractPageUI.DYNAMIC_TEXTAREA, inputValue, textareaName);
	}

	public void selectDynamicDropdownList(String dropdownName, String item) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_DROPDOWN_LIST, dropdownName);
		selectItemInDropdown(driver, AbstractPageUI.DYNAMIC_DROPDOWN_LIST, item, dropdownName);
	}

	public void selectDynamicRadioButton(String radioButtonID) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_RADIO_BUTTON, radioButtonID);
		clickToElement(driver, AbstractPageUI.DYNAMIC_RADIO_BUTTON, radioButtonID);
	}

	public void clickToDynamicButton(String buttonValue) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_BUTTON, buttonValue);
		clickToElement(driver, AbstractPageUI.DYNAMIC_BUTTON, buttonValue);
	}

	public boolean isDynamicMessageDisplay(String message) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_MESSAGE, message);
		return isControlDisplayed(driver, AbstractPageUI.DYNAMIC_MESSAGE, message);
	}
	
	public void openDynamicPage(String pageName) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_PAGE_LINK, pageName);
		clickToElement(driver, AbstractPageUI.DYNAMIC_PAGE_LINK, pageName);
	}

	public void removeDateOfBirthBirthReadOnlyAttribute() {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_TEXTBOX, "dob");
		removeAttributeInDOM(driver, AbstractPageUI.DYNAMIC_TEXTBOX, "type", "dob");
	}

	public void sleep(String timeInSeconds) {
		long time = Long.parseLong(timeInSeconds);
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {

		}
	}

}
