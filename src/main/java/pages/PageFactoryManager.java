package pages;

import org.openqa.selenium.WebDriver;

public class PageFactoryManager {

	public static LoginPageObject openLoginPage(WebDriver driver) {

		return new LoginPageObject(driver);

	}

	public static RegisterPageObject openRegisterPage(WebDriver driver) {

		return new RegisterPageObject(driver);

	}
	
	public static HomePageObject openHomePage(WebDriver driver) {
		
		return new HomePageObject(driver);
		
	}
	
	public static AbstractPageObject openAbstractPage(WebDriver driver) {

		return new AbstractPageObject(driver);

	}

}
